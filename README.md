CAPICHE (ContActs Pigs CorsE). Project NovPATH
================
Facundo Muñoz
June 03, 2021

Objectives:

- Estimation of abundance of wild boar in Corsica.

- Epidemics at the interface between domestic and wild swines in Corsica.

- Analysis of serological samples from wild boar and domestic pigs. Impact of the interaction and co-infections.



## Reports

- [Methodology for the estimation of WB density](https://forgemia.inra.fr/umr-astre/capiche/-/jobs/artifacts/master/raw/reports/Methodology.pdf?job=pages)

  Presents the model for the observed yield and discusses alternatives.
  
- [Wild boar density estimation](https://forgemia.inra.fr/umr-astre/capiche/-/jobs/artifacts/master/raw/reports/WBdensity.pdf?job=pages)

  Preliminary results using only hunting effort.

- [Clean up process of M Laval's data](https://forgemia.inra.fr/umr-astre/capiche/-/jobs/artifacts/master/raw/reports/WBdensity_cleanup.pdf?job=pages)

- [Exploratory analysis of vegetation cover data in Castaniggia](https://forgemia.inra.fr/umr-astre/capiche/-/jobs/artifacts/master/raw/reports/covariable_exploration.pdf?job=pages) 

  Study the predictive power on the harvest adjusted by hunting effort. Null results.

- [Descriptive analysis of hunting data from J Dulat’s internship](https://umr-astre.pages.mia.inra.fr/capiche/description_jdulat.html)

- [Risk-map of pig-contact](https://umr-astre.pages.mia.inra.fr/capiche/Risk_pigs.html)

- [Analysis of interaction surveys](https://umr-astre.pages.mia.inra.fr/capiche/interaction_corsica.html)

- [Modelling co-exposition](https://umr-astre.pages.mia.inra.fr/capiche/modelling_coexposition.html)
