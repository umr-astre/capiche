---
title: "Covariable exploration"
author: "Facundo Muñoz"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
# knit: (function(inputFile, encoding, knit_root_dir) {
#     rmarkdown::render(
#       inputFile,
#       encoding = encoding,
#       knit_root_dir = knit_root_dir,
#       output_dir = "../report",
#       output_format = "pdf_document"
#     )
#   })
output:
  pdf_document:
    template: null
    fig_crop: no
    latex_engine: xelatex
    includes:
      in_header: add_latex_packages.tex
documentclass: cirad
# bibliography: famuvie.bib
---


```{r eval = interactive(), include = FALSE}
source("src/packages.R")
source("src/functions.R")
message("Assuming interactive session")
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  cache = FALSE
)

theme_set(theme_bw())
```


## Introduction


```{r load, include = FALSE}
## Source CBNC (cbnc.oec.fr) (?)
## with recodifications by Caroline
tar_load(
  c(
    vegetation_cover,
    team_areas,
    hunts
  )
)
```


```{r intersection, include = FALSE}
## Geometric intersections between vegetal cover and hunting areas
## Each resulting polygon is both of a given cover and from a given area.
## The table contains variables from both of the original polygons.
vcover_tareas <- 
  st_intersection(vegetation_cover, team_areas) %>% 
  droplevels()

```


```{r example-zoom, fig.cap = "Illustration of vegetal-cover computations using hunting-area 10: Village_Linguizzetta."}

vcover_tareas %>%
  dplyr::filter(team == 10) %>%
  droplevels %>%
  tm_shape() +
  tm_polygons(col = "vcover") +
  tm_layout(legend.width = 10)

```


```{r example-values}
vcover_tareas %>% 
  dplyr::filter(team == 10) %>%
  mutate(
    ., 
    `Recodage.l` = factor(
      suppressWarnings(
        abbreviate(`Recodage.l`, minlength = 50)
      )
    ),
    AREA = as.numeric(set_units(st_area(.), "ha"))
  ) %>% 
  st_drop_geometry() %>% 
  select(`Recodage.l`, AREA) %>% 
  group_by(`Recodage.l`) %>% 
  summarise(
    AREA = sum(AREA)
  ) %>% 
  mutate(
    percent_area = as.numeric(AREA/sum(AREA)*100)
  ) %>% 
  arrange(desc(AREA)) %>% 
  setNames(c("Vegetal cover", "Area (ha)", "Area (%)")) %>% 
  kable(
    format = "latex",
    digits = 1,
    booktabs = TRUE,
    caption = "Variable values for hunting area 10."
  )
  

```



\clearpage

## Data description

```{r harvested-wb, fig.cap = "Marginal distribution of harvested WB per 100 hunters-days (adjusted harvest) in the season"}
hunts %>% 
  ggplot(aes(y)) +
  geom_histogram(bins = 21) +
  labs(x = "Harvested WB per 100 h·d", y = NULL)
```


```{r map-vegetal-cover, fig.width = 10, fig.height = 12, fig.cap = "Spatial distribution of vegetal cover over all hunting-areas in the study."}
tm_shape(vcover_tareas) +
  tm_polygons(col = "vcover") +
  tm_shape(team_areas[1,]) +
  tm_borders() +
  tm_layout(legend.width = 10)
```

```{r dist-vegetal-cover, fig.cap = "Marginal distribution of vegetal cover over all hunting-areas in the study."}
vcover_tareas %>% 
  st_drop_geometry() %>% 
  group_by(vcover) %>% 
  summarise(
    total_area = sum(AREA)
  ) %>% 
  mutate(
    vcover = fct_reorder(vcover, total_area)
  ) %>% 
  ggplot(aes(vcover, total_area)) +
  geom_point() +
  coord_flip() +
  labs(x = "Vegetal cover", y = "Total area (ha)")
```

```{r vegetal-cover-areas}

## Compute the surface devoted to every possible vegetal cover in
## the hunting area utilised by each team.
cover_areas <- 
  vcover_tareas %>% 
  mutate(
    ., 
    AREA = set_units(st_area(.), "ha")
  ) %>% 
  st_drop_geometry() %>% 
  group_by(team, vcover) %>% 
  summarise(
    AREA = sum(AREA)
  ) %>% 
  group_by(team) %>% 
  mutate(
    percent_area = as.numeric(AREA/sum(AREA)*100)
  ) %>% 
  arrange(team, desc(percent_area)) %>% 
  setNames(c("team", "Vegetal cover", "Area (ha)", "Area (%)")) 
  

# ## Check:
# ## All relative areas sum up to 100 % for each team
# cover_areas %>%
#   group_by(team) %>%
#   summarise(ta = sum(`Area (%)`)) %>% 
#   with(., range(ta))

## Not all covers will be present in every hunting area. This 
## introduces some numerical issues in the analysis (computation of 
## log-ratios for compositional data). Thus, we replace zeros by a
## negligible value of area an order of magnitude smaller than the
## area of the smallest observed area.

cover_areas_completed <- 
  cover_areas %>% 
  select(-`Area (%)`) %>% 
  spread(
    `Vegetal cover`,
    `Area (ha)`,
    fill = min(cover_areas$`Area (ha)`) / 10
  ) %>% 
  gather(cover, area, -team) %>% 
  group_by(team) %>% 
  mutate(
    percent_area = as.numeric(area/sum(area)*100)
  )

```


```{r correlation-plot, fig.cap = "Correlations between composition variables of team's hunting-areas.", fig.width = 9, fig.height = 9}

cover_areas_completed %>% 
  ungroup %>% 
  select(-area) %>% 
  ## Abbreviate labels further
  mutate(cover = suppressWarnings(abbreviate(cover, minlength = 12))) %>% 
  spread(cover, percent_area) %>% 
  select(-team) %>% 
  as.matrix() %>% 
  cor() %>% 
  corrplot(method = "ellipse")
  
```


```{r harvest-by-main-cover, fig.cap = "Adjusted harvest of teams by preponderant vegetal cover in their hunting area. Each point is a team."}
hunts %>% 
  inner_join(
    cover_areas %>% 
      group_by(team) %>% 
      top_n(1, `Area (%)`),
    by = "team"
  ) %>%
  mutate(
    cover = fct_reorder(`Vegetal cover`, y)
  ) %>% 
  ggplot(aes(cover, y)) +
  geom_violin() +
  geom_point() +
  coord_flip() +
  labs(x = "Preponderant vegetal cover", y = "Adjusted harvest")
```

```{r harvest-by-percent-cover, fig.width = 10, fig.height = 10, fig.cap = "Adjusted harvest by cover proportion in the hunting area. Within each panel, each point is a team. A team is represented in as many panels as the total number of vegetal species in their hunting area."}

hunts %>% 
  inner_join(
    cover_areas,
    by = "team"
  ) %>%
  ggplot(aes(`Area (%)`, y)) +
  geom_point(aes(col = team)) +
  facet_wrap(~`Vegetal cover`, ncol = 4) +
  scale_colour_viridis_c() +
  # scale_y_continuous(trans = "log10") +
  labs(x = "Area (%)", y = "Adjusted harvest")


```



```{r plot-tests-for-compositional-data, eval = FALSE}

## Barchart for a single team
p1 <-
  hunts %>%
  inner_join(
    cover_areas,
    by = "team"
  ) %>%
  filter(team == 1) %>%
  mutate(Cover = fct_reorder(`Vegetal cover`, `Area (%)`, .desc = TRUE)) %>%
  ggplot(aes(1, `Area (%)`, fill = Cover))

## barchart
p1 + geom_col(position = position_dodge(width = 1))

## piechart
p1 + geom_col(position = "fill", color = "white") + coord_polar(theta = "y")

## wafflechart
wc_size <- 10
wc_df <- expand.grid(x = seq.int(wc_size), y = seq.int(wc_size),
                     KEEP.OUT.ATTRS = FALSE)
categ_table <-
  hunts %>%
  inner_join(
    cover_areas,
    by = "team"
  ) %>%
  filter(team == 1) %>%
  with(
    ., setNames(
      round(`Area (%)` * (wc_size**2)/(sum(`Area (%)`))),
      `Vegetal cover`
    )
  )

sum(categ_table)  # must be 100
wc_df$category <- factor(rep(names(categ_table), categ_table))

ggplot(wc_df, aes(x = x, y = y, fill = category)) +
  geom_tile(color = "black", size = 0.5) +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0), trans = 'reverse') +
  scale_fill_brewer(palette = "Set3") +
  theme(panel.border = element_rect(size = 2),
        plot.title = element_text(size = rel(1.2)),
        axis.text = element_blank(),
        axis.title = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        legend.position = "right")

## treemap
p1$data %>%
  ggplot(aes(area = `Area (%)`, fill = `Vegetal cover`)) +
  geom_treemap(col = "white")

set.seed(20190327)
tibble(x1 = rnorm(5), y1 = rnorm(5)) %>%
  group_by(x1, y1) %>%
  do(tibble(component = LETTERS[1:3], value = runif(3)))  %>%
  mutate(total = sum(value)) %>%
  group_by(x1, y1, total)

```


```{r cover-composition-by-team-performance, fig.cap = "Vegetal cover composition of hunting area by adjusted-harvest of each team. The numbers in the plot are team ids. The axis of ordinates is meaningless. Just a recursive rank for visualisation purposes.", fig.width = 10, fig.height=12}


## The following code has been inspired by
## https://stackoverflow.com/questions/43984614/rggplot2geom-points-how-to-swap-points-with-pie-charts/44125392#44125392

df.grobs <-
  hunts %>% 
  inner_join(
    cover_areas,
    by = "team"
  ) %>%
  mutate(
    quarter = cut(
      y,
      breaks = quantile(y, probs = seq(0, 1, .25)),
      include.lowest = TRUE
    )
  ) %>% 
  mutate(
    team = fct_reorder(as.character(team), y)
  ) %>% 
  group_by(quarter) %>% 
  mutate(
    rank_team = dense_rank(team)
  ) %>% 
  group_by(team, y, quarter, rank_team) %>%
  do(
    # subplots = ggplot(., aes(1, `Area (%)`, fill = `Vegetal cover`)) + 
    #   geom_col(position = position_dodge(width = 1)) +
    #   coord_polar(theta = "y") +
    subplots = ggplot(., aes(area =`Area (%)`, fill = `Vegetal cover`)) + 
      geom_treemap(colour = "white") +
      theme_void() +
      scale_fill_viridis_d(drop = FALSE, guide = "none")
  ) %>% 
  mutate(
    subgrobs = list(
      annotation_custom(
        ggplotGrob(subplots), 
        x = y-1, y = rank_team - 0.5, 
        xmax = y+1, ymax = rank_team + 0.5
      )
    )
  )

# df.grobs$subplots[[1]]

# p <- ggplot(df.grobs$subplots[[1]]$data, aes(y, team)) +
#   geom_point(aes(Inf, Inf), colour = "white") +
#   lims(x = c(0, 100), y = c(0, 64)) 
# p + df.grobs$subgrobs[[1]] + df.grobs$subgrobs[[63]]
df.grobs %>%
{ ggplot(data = ., aes(y, rank_team)) +
    .$subgrobs +
    geom_text(aes(label = team), hjust = "right", nudge_x = -1) +
    geom_col(
      data = cover_areas,
      aes(0, 0, fill = `Vegetal cover`),
      color = "white"
    ) +
    labs(
      x = "Adjusted harvest of the team",
      y = "Recursive rank"
    ) +
    scale_fill_viridis_d(
      drop = FALSE,
      guide = "none"
    ) +
    guides(
      fill = guide_legend(
        "Vegetal cover",
        ncol = 3
      )
    ) +
    theme(
      axis.text.y= element_blank(),
      axis.ticks.y = element_blank(),
      legend.position = "bottom"
    )
}

```

There are two teams from hunts without a corresponding hunting
area (`r paste(setdiff(hunts$team %>% unique(), team_areas$team), collapse = " and ")`).
Similarly, there are `r length(setdiff(team_areas$team, hunts$team %>% unique()))`
hunting areas for which we don't have any yield information.


## Dimensionality reduction

### PCA on the composition variables

```{r hunts_cover_composition}

## 61 "individuals" (teams)
## 27 variables (cover proportions) + 1 response (y: adjusted harvest)
hunts_cover_composition <- 
  hunts %>% 
  inner_join(
    cover_areas_completed,
    by = "team"
  ) %>% 
  ## Wide-form
  dplyr::select(-area) %>% 
  ## Abbreviate labels
  mutate(cover = suppressWarnings(abbreviate(cover, minlength = 12))) %>% 
  spread(cover, percent_area) %>% 
  dplyr::select(-(team:heff))
```

We are conducting the PCA and succesive analyses using only the data
from those teams for which we have the numbers and the territory.
This makes for `r nrow(hunts_cover_composition)` teams.


```{r number-of-factors, fig.cap = "Analysis of the number of latent factors for reduction of dimensionality."}

ev <- eigen(cor(hunts_cover_composition %>% dplyr::select(-y))) 
ap <- parallel(
  subject = nrow(hunts_cover_composition),
  var = ncol(hunts_cover_composition) - 1,
  rep = 100,
  cent = .05
)
nS <- nScree(x=ev$values, aparallel=ap$eigen$qevpea)

plotnScree(nS)

## Optimal Coordinates
## nS$Components$noc
```



```{r PCA}
## PCA of the cover composition proportions in the team areas

vcover_pca <- 
  hunts_cover_composition %>% 
  # mutate_at(vars(-y), scale) %>% ## Unnecessary
  PCA(quanti.sup = 1, ncp = nS$Components$noc, graph = FALSE)

# summary(vcover_pca)
```

```{r dimension-composition}
dimdesc(vcover_pca, axes = seq.int(nS$Components$noc), proba = .01)
```

```{r varimax-rotation, fig.cap = "Varimax-rotation of principal axes."}

loadings <- varimax(vcover_pca$var$coord)$loadings

bind_rows(
as_tibble(vcover_pca$var$coord[, 1:2]) %>% 
  add_column(type = "PCA"),
as_tibble(loadings[, 1:2]) %>% 
  add_column(type = "Varimax-rotated PCA")
) %>% 
  ggplot(aes(`Dim.1`, `Dim.2`, col = type)) +
  geom_point() +
  scale_color_discrete(NULL)
```


```{r rotated-coeffs-1, fig.cap = "Coefficients for the rotated 1st factor"}
loadings[, 1] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```

```{r rotated-coefs-2, fig.cap = "Coefficients for the rotated 2nd factor"}
loadings[, 2] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```



```{r eval = FALSE}
# http://www.karlin.mff.cuni.cz/~maciak/NMST539/cvicenie2017_7.html

fa2 <- factanal(as.matrix(hunts_cover_composition)[, -1], factors = 2, rotation="varimax")
print(fa2, digits=2, cutoff=.1, sort=TRUE)

fa3 <- factanal(as.matrix(hunts_cover_composition)[, -1], factors = 3, rotation="varimax")
print(fa3, digits=2, cutoff=.6, sort=TRUE)

fa6 <- factanal(as.matrix(hunts_cover_composition)[, -1], factors = 6, rotation="varimax")
print(fa6, digits=2, cutoff=.6, sort=TRUE)

load <- fa1$loadings[,1:2] 
plot(load, pch = 20) 
plot(load,type="n") 
text(load,labels=rownames(load),cex=.7)

```


```{r fig:biplots, fig.cap = "Individuals and variables plot from PCA. Labels have been abbreviated for readibility.", fig.width = 10, fig.height = 15}
par(mfcol = c(2, 1))
plot.PCA(vcover_pca, choix = "ind")
# plot.PCA(vcover_pca, choix = "var", invisible = "quanti.sup")
# plot.PCA(vcover_pca, choix = "var", invisible = "var")
plot.PCA(vcover_pca, choix = "var")
# plot.PCA(vcover_pca, axes = c(3, 4), choix = "var")

```


```{r pca-coefs-dim1, eval = FALSE, fig.cap = "Coefficients for the 1st factor"}
vcover_pca$var$coord[, 1] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```

```{r pca-coefs-dim2, eval = FALSE, fig.cap = "Coefficients for the 2nd factor"}
vcover_pca$var$coord[, 2] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```


```{r synthetic-variables}

dat <- 
  bind_cols(
    y = hunts_cover_composition$y,
    as_tibble(
      as.matrix(hunts_cover_composition)[, -1] %*% loadings
    ) %>% 
      set_names(., paste("Cover", seq.int(ncol(.)), sep = "_"))
  )

```

```{r response-vs-variables}

dat %>% 
  rowid_to_column(var = "id") %>% 
  gather(variable, value, -y, -id) %>% 
  ggplot(aes(value, y)) +
  geom_text(aes(label = id)) +
  facet_wrap(~variable, ncol = 2)

```


```{r linear-model-1, eval = FALSE}
fm1 <- lm(y ~ ., data = dat)
# plot(fm1)
## Observation number 58 has a too large influence

# fm2 <- lm(y ~ ., data = dat[-58, ])

dat %>% 
  add_column(pred = predict(fm1)) %>% 
  rownames_to_column(var = "id") %>% 
  ggplot(aes(y, pred)) +
  # geom_point() +
  geom_abline(intercept = 0, slope = 1) +
  geom_text(aes(label = id))

```


### PCA on the inverse-logistic transform of composition variables

```{r hunts_cover_ilc}

## 61 "individuals" (teams)
## 27 variables (cover proportions) + 1 response (y: adjusted harvest)
hunts_cover_ilc <-
  hunts_cover_composition %>% 
  mutate(y = log(y)) %>% 
  mutate_at(vars(-y), ~qlogis(./100))
  
```


```{r number-of-factors-ilc, fig.cap = "Analysis of the number of latent factors for reduction of dimensionality."}

ev_ilc <- eigen(cor(hunts_cover_ilc %>% dplyr::select(-y))) 
ap_ilc <- parallel(
  subject = nrow(hunts_cover_ilc),
  var = ncol(hunts_cover_ilc) - 1,
  rep = 100,
  cent = .05
)
nS_ilc <- nScree(x=ev_ilc$values, aparallel=ap_ilc$eigen$qevpea)

plotnScree(nS_ilc)

## Optimal Coordinates
## nS_ilc$Components$noc
```



```{r PCA-ilc}
## PCA of the cover composition proportions in the team areas

vcover_pca_ilc <- 
  hunts_cover_ilc %>% 
  # mutate_at(vars(-y), scale) %>% ## Unnecessary
  PCA(quanti.sup = 1, ncp = nS_ilc$Components$noc, graph = FALSE)

# summary(vcover_pca)
```

```{r dimension-composition_ilc}
dimdesc(vcover_pca_ilc, axes = seq.int(nS_ilc$Components$noc), proba = .01)
```

```{r varimax-rotation-ilc, fig.cap = "Varimax-rotation of principal axes."}

loadings_ilc <- varimax(vcover_pca_ilc$var$coord)$loadings

bind_rows(
as_tibble(vcover_pca_ilc$var$coord[, 1:2]) %>% 
  add_column(type = "PCA"),
as_tibble(loadings_ilc[, 1:2]) %>% 
  add_column(type = "Varimax-rotated PCA")
) %>% 
  ggplot(aes(`Dim.1`, `Dim.2`)) +
  geom_point() +
  facet_wrap(~type, ncol = 1)
```


```{r rotated-coeffs-1-ilc, fig.cap = "Coefficients for the rotated 1st factor"}
loadings_ilc[, 1] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```

```{r rotated-coefs-2-ilc, fig.cap = "Coefficients for the rotated 2nd factor"}
loadings_ilc[, 2] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```


```{r fig:biplots-ilc, fig.cap = "Individuals and variables plot from PCA. Labels have been abbreviated for readibility.", fig.width = 10, fig.height = 15}
par(mfcol = c(2, 1))
plot.PCA(vcover_pca_ilc, choix = "ind")
# plot.PCA(vcover_pca_ilc, choix = "var", invisible = "quanti.sup")
# plot.PCA(vcover_pca_ilc, choix = "var", invisible = "var")
plot.PCA(vcover_pca_ilc, choix = "var")
# plot.PCA(vcover_pca_ilc, axes = c(3, 4), choix = "var")

```


```{r pca-coefs-dim1-ilc, eval = FALSE, fig.cap = "Coefficients for the 1st factor"}
vcover_pca_ilc$var$coord[, 1] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```

```{r pca-coefs-dim2-ilc, eval = FALSE, fig.cap = "Coefficients for the 2nd factor"}
vcover_pca_ilc$var$coord[, 2] %>% 
  enframe(name = "cover") %>% 
  mutate(
    cover = fct_reorder(cover, value)
  ) %>% 
  ggplot(aes(cover, value)) +
  geom_point() +
  geom_abline(intercept = 0, slope = 0) +
  coord_flip()

```


```{r synthetic-variables-ilc}

dat_ilc <- 
  bind_cols(
    y = hunts_cover_ilc$y,
    as_tibble(
      as.matrix(hunts_cover_ilc)[, -1] %*% loadings_ilc
    ) %>% 
      set_names(., paste("Cover", seq.int(ncol(.)), sep = "_"))
  )

```

```{r response-vs-variables-ilc}

dat_ilc %>% 
  rowid_to_column(var = "id") %>% 
  gather(variable, value, -y, -id) %>% 
  ggplot(aes(value, y)) +
  geom_text(aes(label = id)) +
  facet_wrap(~variable, ncol = 2)

```

```{r}
dat_ilc %>%
  rowid_to_column(var = "id") %>% 
  ggplot(aes(Cover_1, Cover_2, col = y)) +
  geom_point() +
  geom_text(aes(label = id), vjust = -1) +
  scale_colour_viridis_c()
```

```{r linear-model-2, fig.cap = "Prediction vs observations of a linear model of the log-adjusted harvest vs the synthetic environmental covariates."}
fm2 <- lm(y ~ ., data = dat_ilc)
summary(fm2)
# plot(fm2)
## Things improved, but 58 continues to have a large influence, 
## and 60 remains badly explained

# summary(lm(y ~ ., data = dat_ilc[-58, ]))

dat_ilc %>% 
  add_column(pred = predict(fm2)) %>% 
  rownames_to_column(var = "id") %>% 
  ggplot(aes(y, pred)) +
  # geom_point() +
  geom_abline(intercept = 0, slope = 1) +
  geom_text(aes(label = id))

```

```{r gam-ilc, fig.cap = "Prediction vs observations of a regression with non-linear terms."}
fm3 <- gam(
  y ~ s(Cover_1, k = 6) + s(Cover_2, k = 6) + s(Cover_3, k = 6) + s(Cover_4, k = 6) + s(Cover_5, k = 6),# + s(Cover_6, k = 6) + s(Cover_7, k = 6),
  data = dat_ilc
)


# vis.gam(fm3, view = c("Cover_5", "Cover_2"))

## This can overfit a little.
## See the fit from these:
## Even their AIC improves a lot.
# fm4 <- gam(
#   y ~ s(Cover_1, Cover_2, Cover_3, Cover_4, k = 50), data = dat_ilc
# )
# fm5 <- gam(
#   y ~ s(Cover_1, Cover_2, Cover_3, Cover_4, k = 25) +
#     s(Cover_5, Cover_6, Cover_7, k = 25), data = dat_ilc
# )

dat_ilc %>% 
  add_column(pred = predict(fm3)) %>% 
  rownames_to_column(var = "id") %>% 
  ggplot(aes(y, pred)) +
  # geom_point() +
  geom_abline(intercept = 0, slope = 1) +
  geom_text(aes(label = id))

```

```{r model-comparison, echo = TRUE}
AIC(fm2, fm3)
```

\clearpage

## TODO

- PCA of compositional variables and outcome (as a supplementary variable)

- PLS of outcome vs compositional variables

- MCA of presence/abscence of species (outcome as supplementary)

- FAMD simultaneous analysis of quantitative (proportions) and qualitative (presence) variables

- In all of the precedent analyses, use either proportions or compositional-coordinates for the quantitative variables.



## Conclusions

- One team super-effective (outlier) (Fig. 2, 6, 7)

- Most of the aggregated hunting territory is of type "Petits bois..." (Fig 4). 
However, this is not the most typical cover in all hunting territories (Fig. 5).
Reason: this type of vegetal cover is concentrated in the east (Fig. 3).

- Lots of variability in performance with respect to the typical species
in the hunting region (Fig. 5). Maybe the most abundant species in the region 
is not necessarily where people hunt or wildboar live?

    I don't see a convincing signal here.
    
- I fail to see any pattern either from figures 6 or 7.


## ANNEX

### Vegetal cover labels and their original definition

Original vegetal cover categories have been aggregated into a smaller 
number of classes, and relabelled for readaility.

```{r table-vcover-classes}
vegetation_cover %>% 
  st_drop_geometry() %>% 
  select(vcover, Definition) %>% 
  unique() %>% 
  arrange(vcover) %>% 
  kable(format = "latex", longtable = TRUE, booktabs = TRUE) %>% 
  kable_styling(font_size = 9) %>% 
  column_spec(2, width = "28em") %>% 
  collapse_rows(1, latex_hline = "major")
  
  # This uses html
  # flextable() %>% 
  # autofit() %>% 
  # width(., j = 2, width = 7) %>% 
  # knit_print() 

# dim(autofit(ft))
```

