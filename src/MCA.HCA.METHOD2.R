
###### MCA et HCA

########## CHARGEMENT BASE DONNEES

# setwd("C:/Users/ldupon/Desktop/Stage Liane/Bases de donn?es/BD Analyse")

DataPORCS = read.csv("DATA_PORC2.csv",sep=";")

DATA <- DataPORCS[1:103, 2:24]

summary(DataPORCS)
names(DataPORCS)

######## INSTALLATION PACKAGES

install.packages(c("FactoMineR", "factoextra", "missMDA"))
library("FactoMineR")
library("factoextra")
library(PCAmixdata)


########################################################################################

########## GESTION DONNEES MANQUANTES

library('missMDA')
nb = estim_ncpMCA(DATA,ncp.max=5)
nb
tab.disj.comp = imputeMCA(DATA, ncp=5)$tab.disj

#############################################################################

######################## realisation MCA


res.mca = MCA(DATA, quali.sup = c(1:7), tab.disj=tab.disj.comp)

plot(res.mca, axes = c(1, 2), choix=c("var"))
plot(res.mca, axes = c(1, 3), choix=c("var"), col.quali.sup = "darkgreen")


######### eigen values

library("factoextra")
eig.val <- get_eigenvalue(res.mca)

fviz_screeplot (res.mca, addlabels = TRUE, ylim = c (0, 45))


######## variables

var <- get_mca_var(res.mca)

######### contribution des variables aux dimensions
# Contributions des variables ? la dimension 1
fviz_contrib (res.mca, choice = "var", axes = 1, top = 15)
# Contributions des variables ? la dimension 2
fviz_contrib (res.mca, choice = "var", axes = 2, top = 15)
# Contributions des variables ? la dimension 3
fviz_contrib (res.mca, choice = "var", axes = 3, top = 15)


#########individus

ind <- get_mca_ind (res.mca)
ind
# Coordonn?es
head(ind$coord)
# Qualit? de representation
head(ind$cos2)
# Contributions
head(ind$contrib)

fviz_mca_ind (res.mca,
              #label = "none", # masquer le texte des individus
              habillage = "Animaux.en.libert?", # colorer par groupes
              palette = c ("#00AFBB", "#E7B800", "#FC4E07", "blue", "red", "yellow"),
              addEllipses = TRUE, ellipse.type = "confidence",
              ggtheme = theme_minimal ())

###########################################################
################### CLUSTER : Ward Hierarchical Clustering

res.hcpc <- HCPC(res.mca, nb.clust = 0, min = 3, max = NULL, graph = TRUE)

#### SECTION POUR AVOIR 4 CLUSTERS


plot(res.hcpc, choice = "3D.map", axes = c(1, 2))
plot(res.hcpc, choice = "3D.map", axes = c(1, 3))

plot(res.hcpc, choice = "tree", axes = c(1, 2))

plot(res.hcpc, choice = "bar", axes = c(1, 2))

plot(res.hcpc, choice = "map", axes = c(1, 2), draw.tree=FALSE)
plot(res.hcpc, choice = "map", axes = c(1, 3), draw.tree=FALSE)

# Individuals factor map
fviz_cluster(res.hcpc, geom = "point", main = "Factor map", axes = c(1, 3))

# Description by variables
res.hcpc$desc.var$test.chi2

# Description by variable categories
res.hcpc$desc.var$category

# Description par principale component
res.hcpc$desc.axes

# Description par individu
res.hcpc$desc.ind$para


